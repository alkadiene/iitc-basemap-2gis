// ==UserScript==
// @id             iitc-basemap-2gis
// @name           IITC plugin: 2GIS map tiles
// @author         @l1bbcsg
// @category       Map Tiles
// @version        0.0.2
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/l1bbcsg/iitc-basemap-2gis/raw/master/iitc-basemap-2gis.user.js
// @description    Adds 2GIS basemap layer
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

(function() {
	var NAME = 'iitc-basemap-2gis';
	var VERSION = '0.0.2.20171111';

	if(typeof window.plugin !== 'function') {
		window.plugin = function() {};
	}

	window.plugin.mapTile2gis = function() {};

	var setup = window.plugin.mapTile2gis.setup = function () {
		layerChooser.addBaseLayer(new L.TileLayer(
			'https://tile{s}.maps.2gis.com/tiles?x={x}&y={y}&z={z}&v=1.3',
			{
				attribution: 'Map tiles stolen from <a href="http://info.2gis.ru/">2GIS</a>',
				subdomains: '0123',
			}
		), "2GIS");
	};

	setup.info = {
		buildName: NAME + ':' + VERSION,
		dateTimeVersion: VERSION,
		pluginId: NAME,
	};

	if(!window.bootPlugins) {
		window.bootPlugins = [];
	}
	window.bootPlugins.push(setup);

	if(window.iitcLoaded && typeof setup === 'function') {
		setup();
	}
})();
